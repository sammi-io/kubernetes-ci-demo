FROM golang:1.11-alpine AS build
WORKDIR /opt/src/app
COPY *.go .
RUN go build -o server

FROM alpine:3.8
WORKDIR /opt/app
COPY --from=build /opt/src/app/server .
ENTRYPOINT [ "/opt/app/server" ]
EXPOSE 80
