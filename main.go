package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {
	port := os.Getenv("PORT")

	if port == "" {
		port = "80"
	}

	addr := fmt.Sprintf(":%s", port)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		log.Printf("[%s %s] handling connection %s\n", r.Method, r.URL.Path, r.RemoteAddr)
		w.Write([]byte("hello, world!"))
	})

	log.Printf("about to start server on: %s\n", addr)

	log.Fatal(http.ListenAndServe(addr, nil))
}
